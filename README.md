CMSIS : Cortex Microcontroller Software Interface Standard
==========================================================

This repository includes the CMSIS released with the LPCXpresso package. The code is provided by NXP and licensed by them as well - it is provided here only for accessibility and to track a few minor bug fixes.
